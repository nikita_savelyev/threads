#include "lecture1_task3.h"

void Barber::sleep() {
	while (!end_of_day || current_queue_size > 0)
		if (current_queue_size != 0)
			work();
}
void Barber::work() {
	access_mutex.lock();
	int id = visitor_queue.front();
	visitor_queue.pop();
	current_queue_size--;
	access_mutex.unlock();

	boost::this_thread::sleep(boost::posix_time::milliseconds(workTime));
	if (id != -1)
		cout << id << "th visitor serviced\n";
	else
		end_of_day = true;
}
Barber::Barber(int _max) {
	end_of_day = false;
	current_queue_size = 0;
	max_queue_size = _max;
}
bool Barber::new_visitor(int id) {
	access_mutex.lock();
	bool res = false;
	if (current_queue_size < max_queue_size) {
		visitor_queue.push(id);
		current_queue_size++;
		res = true;
	}
	access_mutex.unlock();
	return res;
}
void Barber::start() {
	thr = boost::thread(&Barber::sleep, this);
}
void Barber::join() {
	thr.join();
}


const int number_of_visitors = 20,
	number_of_free_places = 10;


void show_lecture1_task3() {
	Barber b(number_of_free_places);
	b.start();

	for (int i = 0; i < number_of_visitors; i++) {
		b.new_visitor(i + 1);
		boost::this_thread::sleep(boost::posix_time::milliseconds(50));
	}
	while (!b.new_visitor(-1));

	b.join();
}