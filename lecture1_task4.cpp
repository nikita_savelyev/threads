#include "lecture1_task4.h"


const int taking_time = 100,
	putting_time =100,
	eating_time = 200,
	life_time = 30*1000;


Table::Table(int k) {
	forks = new int[k];
	for (int i = 0; i < k; i++)
		forks[i] = 0;
	forks_mutex = new mutex[k];
}
bool Table::take(int fork, int id) {
	if (forks_mutex[fork].try_lock()) {
		if (forks[fork] == 0) {
			boost::this_thread::sleep(boost::posix_time::milliseconds(taking_time));
			forks[fork] = id;
			forks_mutex[fork].unlock();
			return true;
		}
		forks_mutex[fork].unlock();
	}
	return false;
}
bool Table::put(int fork) {
	if (forks_mutex[fork].try_lock()) {
		boost::this_thread::sleep(boost::posix_time::milliseconds(taking_time));
		forks[fork] = 0;
		forks_mutex[fork].unlock();
		return true;
	}
	return false;
}


void Philosopher::put_left_fork() {
	while (!table->put(id_of_left_fork));
	owns_left_fork = false;
}
void Philosopher::put_right_fork() {
	while (!table->put(id_of_right_fork));
	owns_right_fork = false;
}
void Philosopher::eat() {
	boost::this_thread::sleep(boost::posix_time::milliseconds(eating_time));
	console_mutex->lock();
	cout << id << "th philosopher ate\n";
	console_mutex->unlock();
}
void Philosopher::start_up() {
	boost::posix_time::ptime start_time = boost::posix_time::second_clock::local_time();
	while (boost::posix_time::second_clock::local_time() - start_time < boost::posix_time::milliseconds(life_time)) {
		if (owns_left_fork && owns_right_fork) {
			eat();
			put_left_fork();
			put_right_fork();
			continue;
		}
		if (!owns_left_fork)
			owns_left_fork = table->take(id_of_left_fork, id);
		else {
			random_device rd;
			if (rd() % 3 == 0)
				put_left_fork();
		}
		if (!owns_right_fork)
			owns_right_fork = table->take(id_of_right_fork, id);
		else {
			random_device rd;
			if (rd() % 3 == 0)
				put_right_fork();
		}
	}
}
Philosopher::Philosopher(int _id, int l, int r, Table* t, mutex* _mtx) {
	table = t;
	id = _id;
	id_of_left_fork = l;
	id_of_right_fork = r;
	owns_left_fork = false;
	owns_right_fork = false;
	console_mutex = _mtx;
	thr = new boost::thread;
}
void Philosopher::start() {
	*thr = boost::thread(&Philosopher::start_up, this);
}
void Philosopher::join() {
	thr->join();
}


const int table_size = 5;


void show_lecture1_task4() {
	mutex* console_mutex = new mutex();

	Table table(table_size);
	vector <Philosopher> philosophers;
	for (int i = 0; i < table_size; i++) {
		Philosopher ph(i + 1, i, (i + 1) % table_size, &table, console_mutex);
		philosophers.push_back(ph);
	}

	for (int i = 0; i < table_size; i++)
		philosophers[i].start();

	for (int i = 0; i < table_size; i++)
		philosophers[i].join();
}