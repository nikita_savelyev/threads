#include "lecture1_task2.h"

History::History(bool _p, int _id, int _prg) {
	push = _p;
	id = _id;
	program = _prg;
}
void History::show() {
	cout << "\t" << id << "th ";
	if (push)
		cout << "developer added ";
	else
		cout << "user took ";
	cout << program << "th program\n";
}
History makeHistory(bool _p, int _id, int _prg) {
	History h(_p, _id, _prg);
	return h;
}


Storage::Storage() {
	mtx = new timed_mutex();
}
bool Storage::push(int program, int id) {
	if (mtx->try_lock()) {
		boost::this_thread::sleep(boost::posix_time::milliseconds(work_time));
		programs.push(program);
		if (program != -1)
			history.push_back(makeHistory(true, id, program));

		mtx->unlock();
		return true;
	}
	return false;
}
bool Storage::pop(int* program, int wait_time, int id) {
	if (mtx->try_lock_for(chrono::milliseconds(wait_time))) {
		boost::this_thread::sleep(boost::posix_time::milliseconds(work_time));

		if (programs.size() == 0) {
			mtx->unlock();
			return false;
		}

		*program = programs.front();
		if (programs.front() != -1)
			programs.pop();
		if (*program != -1)
			history.push_back(makeHistory(false, id, *program));

		mtx->unlock();
		return true;
	}
	return false;
}
void Storage::show(int id) {
	cout << "History of " << id << "th storage:\n";
	for (auto it : history)
		it.show();
}


void Developer::add(int program) {
	int i = 0;
	bool running = true;

	while (running) {
		running = !(*storages)[i].push(program, id);
		i = (i + 1) % storages->size();
	}
}
void Developer::work() {
	bool running = true;
	while (running) {
		int program = -1;

		programs_mutex->lock();
		if (programs->size() == 0)
			running = false;
		else {
			program = programs->front();
			programs->pop();
		}
		programs_mutex->unlock();

		add(program);
	}
}
Developer::Developer(vector <Storage>* stgs, queue <int>* prg, int _id, mutex* _mtx) {
	id = _id;
	programs = prg;
	storages = stgs;
	thr = new boost::thread;
	programs_mutex = _mtx;
}
void Developer::start() {
	*thr = boost::thread(&Developer::work, this);
}
void Developer::join() {
	thr->join();
}


int User::get() {
	int i = 0, program = -2;
	bool running = true;

	while (running) {
		running = !(*storages)[i].pop(&program, wait_time, id);
		if (program == -1) {
			if (!empty_storages[i]) {
				empty_storages[i] = true;
				number_of_empty_storages++;
			}
			if (number_of_empty_storages == storages_size)
				running = false;
			else
				running = true;
		}
		
		i = (i + 1) % storages_size;
	}

	return program;
}
void User::work() {
	bool running = true;
	while (running) {
		int program = get();
		if (number_of_empty_storages == storages_size)
			running = false;
	}
}
User::User(vector <Storage>* stgs, int _id, int w_t) {
	id = _id;
	wait_time = w_t;
	thr = new boost::thread;

	storages = stgs;
	storages_size = stgs->size();
	empty_storages = new bool[storages_size];
	number_of_empty_storages = 0;
	for (int i = 0; i < storages_size; i++)
		empty_storages[i] = false;
}
void User::start() {
	*thr = boost::thread(&User::work, this);
}
void User::join() {
	thr->join();
}

const int number_of_storages = 10,
number_of_developers = 5,
number_of_users = 50,
number_of_programs = 100;


void show_lecture1_task2() {
	mutex* programs_mutex = new mutex();

	queue <int> programs;
	for (int i = 1; i <= number_of_programs; i++)
		programs.push(i);

	vector <Storage> storages(number_of_storages);
	Developer* devs[number_of_developers];
	User* users[number_of_users];

	for (int i = 0; i < number_of_developers; i++)
		devs[i] = new Developer(&storages, &programs, i + 1, programs_mutex);
	for (int i = 0; i < number_of_users / 2; i++)
		users[i] = new User(&storages, i + 1);
	for (int i = number_of_users / 2; i < number_of_users; i++)
		users[i] = new User(&storages, i + 1, 3);

	for (int i = 0; i < number_of_developers; i++)
		devs[i]->start();
	for (int i = 0; i < number_of_users; i++)
		users[i]->start();

	for (int i = 0; i < number_of_developers; i++)
		devs[i]->join();
	for (int i = 0; i < number_of_users; i++)
		users[i]->join();

	for (int i = 0; i < number_of_storages; i++)
		storages[i].show(i + 1);
}