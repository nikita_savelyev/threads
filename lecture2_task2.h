#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

class Security {
private:
	boost::thread* thr;
	void work();
public:
	Security();
	void start();
	void join();
};

class Programmer {
private:
	int id;
	boost::thread* thr;
	void work();
public:
	Programmer(int _id);
	void start();
	void join();
};

void show_lecture2_task2();