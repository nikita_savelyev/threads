#include "lecture2_task2.h"

const int number_of_programmers = 10;

bool TL_has_come = false;
condition_variable security_cv, programmers_cv;
mutex security_mutex, programmers_mutex;

bool isReady() {
	return TL_has_come;
}

void Security::work() {
	unique_lock <mutex> lck(security_mutex);
	security_cv.wait(lck, isReady);
	cout << "Security notifies\n";
	programmers_cv.notify_all();
}
Security::Security() {
	thr = new boost::thread;
}
void Security::start() {
	*thr = boost::thread(&Security::work, this);
}
void Security::join() {
	thr->join();
}

Programmer::Programmer(int _id) {
	id = _id;
	thr = new boost::thread;
}
void Programmer::work() {
	unique_lock <mutex> lck(programmers_mutex);
	programmers_cv.wait(lck);
	cout << id << "stopped playing\n";
}
void Programmer::start() {
	*thr = boost::thread(&Programmer::work, this);
}
void Programmer::join() {
	thr->join();
}

void show_lecture2_task2() {
	Security security;
	vector <Programmer> programmers;
	for (int i = 0; i < number_of_programmers; i++)
		programmers.emplace_back(i);

	security.start();
	for (int i = 0; i < number_of_programmers; i++)
		programmers[i].start();

	TL_has_come = true;
	security_cv.notify_one();

	security.join();
	for (int i = 0; i < number_of_programmers; i++)
		programmers[i].join();
}