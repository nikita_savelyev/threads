#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

stack <time_t> time_stack;

void startTimer() {
	time_stack.push(clock());
}
double stopTimer() {
	double time = double(clock()) - time_stack.top();
	time_stack.pop();
	return time / double(CLOCKS_PER_SEC);
}