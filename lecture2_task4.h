#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

struct FileData {
	string s;
	int number_of_symbols[256];
	FileData();
};

string readFile(string file_name);

FileData combineStrings(vector <string> v);

vector <vector <int> > findSubstrings(vector <string> v);

void show_lecture2_task4();