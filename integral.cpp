#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif
#include "integral.h"

double f(double x) {
	return -(x - 9)*(x - 9)*(x - 9) - 5 * sqrt(x);
}

void Integral::add_square(int id) {
	double res = 0,
		l = a + eps*id,
		r = a + eps*(id + 1),
		mid;
	while (r <= b) {
		mid = (l + r) / 2;
		res += (r - l)*function(mid);
		l += eps*number_of_threads;
		r += eps*number_of_threads;
	}
	answer_mutex.lock();
	answer += res;
	answer_mutex.unlock();
}
void Integral::start_thread(int id) {
	threads.push_back(boost::thread(&Integral::add_square, this, id));
}
void Integral::join_thread() {
	threads.rbegin()->join();
	threads.pop_back();
}
Integral::Integral(double(*_f)(double)) {
	function = _f;
}
double Integral::calculate(double _a, double _b, double _eps, int _n) {
	a = _a;
	b = _b;
	eps = _eps;
	number_of_threads = _n;

	for (int i = 0; i < number_of_threads; i++)
		start_thread(i);

	for (int i = 0; i < number_of_threads; i++)
		join_thread();

	return answer;
}
double Integral::get_ans() {
	return answer;
}

const double EPS = 1e-8;
const int NUM_OF_THREADS = 4;

void show_integral() {
	startTimer();

	Integral integral(f);

	cout.precision(12);
	cout << fixed << integral.calculate(1, 5, EPS, NUM_OF_THREADS) << endl;
	cout << "Calculating time: " << stopTimer() << "s\n";
}