#include "lecture2_task3.h"

const int queue_length = 100000;

bool waitInQueue() {
	random_device rd;
	int random_value;

	do
		random_value = rd();
	while (random_value % queue_length > 1);

	return random_value % queue_length == 0;
}

void getPassport() {
	future <bool> result = async(waitInQueue);
	int counter = 0;
	while (!result.get()) {
		counter++;
		result = async(waitInQueue);
	}
	cout << "Success after " << counter << " visits\n";
}

void show_lecture2_task3() {
	getPassport();
}
