#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

class Table {
private:
	int* forks;
	mutex* forks_mutex;
public:
	Table(int k);
	bool take(int fork, int id);
	bool put(int fork);
};

class Philosopher {
private:
	int id;
	int id_of_left_fork, id_of_right_fork;
	bool owns_left_fork, owns_right_fork;
	boost::thread* thr;
	Table* table;
	mutex* console_mutex;

	void put_left_fork();
	void put_right_fork();
	void eat();
	void start_up();
public:
	Philosopher(int _id, int l, int r, Table* t, mutex* mtx);
	void start();
	void join();
};


void show_lecture1_task4();