#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

const int workTime = 200;

class Barber {
private:
	queue <int> visitor_queue;
	int current_queue_size;
	int max_queue_size;
	boost::thread thr;
	mutex access_mutex;
	bool end_of_day;

	void sleep();
	void work();
public:
	Barber(int _max);
	bool new_visitor(int id);
	void start();
	void join();
};


void show_lecture1_task3();