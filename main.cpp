#ifndef _INCLUDES_
#define INCLUDES
	#include "Includes.h"
#endif
#include "integral.h"
#include "lecture1_task2.h"
#include "lecture1_task3.h"
#include "lecture1_task4.h"
#include "lecture2_task2.h"
#include "lecture2_task3.h"
#include "lecture2_task4.h"


int main() {
	startTimer();

	//show_lecture1_task2();
	//show_lecture1_task3();
	//show_lecture1_task4();
	//show_integral();
	//show_lecture2_task2();
	//show_lecture2_task3();
	show_lecture2_task4();

	cout << "Working time: " << stopTimer() << "s\n";
	system("pause");
	return 0;
}