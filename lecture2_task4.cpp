#include "lecture2_task4.h"

FileData::FileData() {
	s = "";
	for (int i = 0; i < 256; i++)
		number_of_symbols[i] = 0;
}

string readFile(string file_name) {
	ifstream in(file_name);
	string res = "";
	char c;
	while (in.get(c))
		res.push_back(c);
	return res;
}

FileData combineStrings(vector <string> files_contents) {
	FileData res;
	for (size_t i = 0; i < files_contents.size(); i++) {
		unsigned char c;
		for (size_t j = 0; j < files_contents[i].size(); j++) {
			c = files_contents[i][j];
			res.s += c;
			res.number_of_symbols[c]++;
		}

		c = '\n';
		res.s += c;
		res.number_of_symbols[c]++;
	}
	return res;
}

vector <vector <int> > findSubstrings(vector <string> v, string find) {
	vector <vector <int> > res(v.size());
	for (size_t i = 0; i < v.size(); i++) {
		string s = find + '\0' + v[i];
		int *p = new int[s.size()];
		p[0] = 0;
		for (size_t j = 1; j < s.size(); j++) {

			int t = p[j - 1];
			while (t > 0 && s[j] != s[t])
				t = p[t - 1];
			if (s[j] == s[t])
				t++;
			p[j] = t;

			if (p[j] == find.size())
				res[i].push_back(j - 2*find.size() + 1);
		}
	}
	return res;
}

const int number_of_files = 3;
const string input = "input";
const string to_find = "�������������";

void show_lecture2_task4() {
	
	future <string> file_futures[number_of_files];
	for (int i = 0; i < number_of_files; i++) {
		string file_name = input + to_string(i + 1) + ".txt";
		file_futures[i] = async(readFile, file_name);
	}

	vector <string> files_data(number_of_files);
	for (int i = 0; i < number_of_files; i++)
		files_data[i] = file_futures[i].get();


	ofstream out("output.txt");
	FileData result = combineStrings(files_data);
	out << "Statistics:\n";
	for (int i = 0; i < 256; i++)
		out << '\'' << char(i) << '\'' << " : " << result.number_of_symbols[i] << endl;
	
	out << "\nCombined file:\n";
	for (size_t i = 0; i < result.s.size(); i++)
		out << result.s[i];

	vector <vector <int> > found = findSubstrings(files_data, to_find);
	out << to_find << " was found in:\n";
	for (int i = 0; i < number_of_files; i++)
		if (found[i].size() > 0) {
			out << i + 1 << "th file: ";
			for (size_t j = 0; j < found[i].size(); j++)
				out << found[i][j] << " ";
			out << endl;
		}

}