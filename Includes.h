#include <iostream>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <stack>
#include <mutex>
#include <vector>
#include <random>
#include <queue>
#include <cmath>
#include <ctime>
#include <condition_variable>
#include <future>
#include <string>
using namespace std;

void startTimer();
double stopTimer();