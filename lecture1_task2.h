#ifndef INCLUDES
#define INCLUDES
	#include "Includes.h"
#endif

const int work_time = 10;
const int inf_time = 1000000;

struct History {
	bool push;
	int id;
	int program;
	History(bool _p, int _id, int _prg);
	void show();
};

History makeHistory(bool _p, int _id, int _prg);

class Storage {
private:
	queue <int> programs;
	timed_mutex* mtx;
	vector <History> history;
public:
	Storage();
	bool push(int program, int id);
	bool pop(int* program, int wait_time, int id);
	
	void show(int id);
};


class Developer {
private:
	vector <Storage>* storages;
	queue <int>* programs;
	int id;
	boost::thread* thr;
	mutex* programs_mutex;

	void add(int program);
	void work();
public:

	Developer(vector <Storage>* stgs, queue <int>* prg, int _id, mutex* _mtx);
	void start();
	void join();
};


class User {
private:
	int wait_time;
	int id;
	int storages_size;
	int number_of_empty_storages;
	vector <Storage>* storages;
	bool* empty_storages;
	boost::thread* thr;

	int get();
	void work();
public:
	User(vector <Storage>* stgs, int _id, int w_t = inf_time);
	void start();
	void join();
};

void show_lecture1_task2();