#ifndef _INCLUDES_
#define INCLUDES
#include "Includes.h"
#endif

class Integral {
private:
	double(*function)(double);
	double a, b;
	double eps;
	double answer;
	int number_of_threads;
	mutex answer_mutex;
	vector <boost::thread> threads;

	void add_square(int id);
	void start_thread(int id);
	void join_thread();
public:
	Integral(double(*_f)(double));
	double calculate(double _a, double _b, double _eps, int _n);
	double get_ans();
};

void show_integral();